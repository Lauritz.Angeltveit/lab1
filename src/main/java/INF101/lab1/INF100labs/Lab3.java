package INF101.lab1.INF100labs;

import javax.xml.stream.events.EndElement;

/**
 * Implement the methods multiplesOfSevenUpTo, multiplicationTable and crossSum.
 * These programming tasks was part of lab3 in INF100 fall 2022. You can find them here: https://inf100.ii.uib.no/lab/3/
 */
public class Lab3 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs

    }

    public static void multiplesOfSevenUpTo(int n) {
        for (int i = 1; i<=n; i++) {
            if (i%7 == 0) {
                System.out.println(i);
            }
          }
    }

    public static void multiplicationTable(int n) {
        for (int rad = 1; rad <= n; rad++) {
            System.out.print(rad + ": ");
            for (int kolonne = 1; kolonne <= n; kolonne++) {
                int gange = kolonne*rad;
                System.out.print(gange + " ");
            }
            System.out.println();
        }
    }

    public static int crossSum(int num) {
        int sum = 0;
        while (num>0) {
            sum += num%10;
            num/=10;
        }
        return sum;
    }

}