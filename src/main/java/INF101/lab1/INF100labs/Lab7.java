package INF101.lab1.INF100labs;

import java.util.ArrayList;

/**
 * Implement the methods removeRow and allRowsAndColsAreEqualSum.
 * These programming tasks was part of lab7 in INF100 fall 2022. You can find
 * them here: https://inf100.ii.uib.no/lab/7/
 */
public class Lab7 {

    public static void main(String[] args) {
        // Call the methods here to test them on different inputs

    }

    public static void removeRow(ArrayList<ArrayList<Integer>> grid, int row) {
        grid.remove(row);
    }

    public static boolean allRowsAndColsAreEqualSum(ArrayList<ArrayList<Integer>> grid) {

        int[] rowCount = new int[grid.size()];
        int[] colCount = new int[grid.get(0).size()];
        
            for (int i = 0; i < grid.size(); i++) {
                int sumRow = 0;
                for (int j = 0; j < grid.get(0).size(); j++) {
                    sumRow += grid.get(i).get(j);
                }
                rowCount[i] = sumRow;
            }
        
            for (int s = 0; s < grid.get(0).size(); s++) {
                int sumCol = 0;
                for (int u = 0; u < grid.size(); u++) {
                    sumCol += grid.get(u).get(s);
                }
                colCount[s] = sumCol;
            }
        
            int firstElementRow = rowCount[0];
            int firstElementCol = colCount[0];
        
            for (int k = 0; k < rowCount.length; k++) {
                if (firstElementRow != rowCount[k]) {
                    return false;
                }
            }
        
            for (int h = 0; h < colCount.length; h++) {
                if (firstElementCol != colCount[h]) {
                    return false;
                }
            }
            return true;
        }
        
    }

